/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.ResultSet;

/**
 *
 * @author Pablo
 */
public class Entrenador{
    private int id_entrenador;
    private String nombre_entrenador;
    private String campo_entrenador;
    private int num_jugadores;
    private int num_balones;

    public Entrenador(int id_entrenador, String nombre_entrenador, String campo_entrenador, int num_jugadores, int num_balones) {
        this.id_entrenador = id_entrenador;
        this.nombre_entrenador = nombre_entrenador;
        this.campo_entrenador = campo_entrenador;
        this.num_jugadores = num_jugadores;
        this.num_balones = num_balones;
    }

    @Override
    public String toString() {
        return "Entrenador{" + "id_entrenador=" + id_entrenador + ", nombre_entrenador=" + nombre_entrenador + ", campo_entrenador=" + campo_entrenador + ", num_jugadores=" + num_jugadores + ", num_balones=" + num_balones + '}';
    }

    public int getId_entrenador() {
        return id_entrenador;
    }

    public void setId_entrenador(int id_entrenador) {
        this.id_entrenador = id_entrenador;
    }

    public String getNombre_entrenador() {
        return nombre_entrenador;
    }

    public void setNombre_entrenador(String nombre_entrenador) {
        this.nombre_entrenador = nombre_entrenador;
    }

    public String getCampo_entrenador() {
        return campo_entrenador;
    }

    public void setCampo_entrenador(String campo_entrenador) {
        this.campo_entrenador = campo_entrenador;
    }

    public int getNum_jugadores() {
        return num_jugadores;
    }

    public void setNum_jugadores(int num_jugadores) {
        this.num_jugadores = num_jugadores;
    }

    public int getNum_balones() {
        return num_balones;
    }

    public void setNum_balones(int num_balones) {
        this.num_balones = num_balones;
    }

    
    
}
