/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.ResultSet;

/**
 *
 * @author Pablo
 */
public interface EntrenarDOD {

    public ResultSet selectAllEntrenar();
    public EntrenarDOD insertEntrenar(int id_jugador, int id_entrenador);
    public boolean deleteEntrenar(int id_jugador, int id_entrenador);
    public EntrenarDOD updateEntrenar(int id_jugador, int id_entrenador);
    // extra
    public ResultSet selectRegion(int id_jugador, int id_entrenador);
    
}
