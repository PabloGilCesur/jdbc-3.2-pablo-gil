/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.ResultSet;

/**
 *
 * @author Pablo
 */
public interface EntrenadorDOD {

    public ResultSet selectAllEntrenador();
    public EntrenadorDOD insertEntrenador(int id_entrenador, String nombre_entrenador, String campo_entrenador, int num_jugadores, int num_balones);
    public boolean deleteEntrenador(int id_entrenador);
    public EntrenadorDOD updateEntrenador(String nombre_entrenador);
    // extra
    public ResultSet selectEntrenador(String nombre_entrenador);
    
}
