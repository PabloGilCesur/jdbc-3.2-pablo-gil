/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.ResultSet;

/**
 *
 * @author Pablo
 */
public class Entrenar {
    private int id_jugador;
    private int id_entrenador;

    public Entrenar(int id_jugador, int id_entrenador) {
        this.id_jugador = id_jugador;
        this.id_entrenador = id_entrenador;
    }

    @Override
    public String toString() {
        return "Entrenar{" + "id_jugador=" + id_jugador + ", id_entrenador=" + id_entrenador + '}';
    }

    public int getId_jugador() {
        return id_jugador;
    }

    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }

    public int getId_entrenador() {
        return id_entrenador;
    }

    public void setId_entrenador(int id_entrenador) {
        this.id_entrenador = id_entrenador;
    }

    
    
}
