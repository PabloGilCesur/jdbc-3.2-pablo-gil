/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.ResultSet;

/**
 *
 * @author Pablo
 */
public class Jugador{
    private int id_jugador;
    private String nombre_jugador;
    private String apellido_jugador;
    private int tel_jugador;
    private int dias_entrenados;

    public Jugador(int id_jugador, String nombre_jugador, String apellido_jugador, int tel_jugador, int dias_entrenados) {
        this.id_jugador = id_jugador;
        this.nombre_jugador = nombre_jugador;
        this.apellido_jugador = apellido_jugador;
        this.tel_jugador = tel_jugador;
        this.dias_entrenados = dias_entrenados;
    }    

    @Override
    public String toString() {
        return "Jugador{" + "id_jugador=" + id_jugador + ", nombre_jugador=" + nombre_jugador + ", apellido_jugador=" + apellido_jugador + ", tel_jugador=" + tel_jugador + ", dias_entrenados=" + dias_entrenados + '}';
    }
    
    public int getId_jugador() {
        return id_jugador;
    }

    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }

    public String getNombre_jugador() {
        return nombre_jugador;
    }

    public void setNombre_jugador(String nombre_jugador) {
        this.nombre_jugador = nombre_jugador;
    }

    public String getApellido_jugador() {
        return apellido_jugador;
    }

    public void setApellido_jugador(String apellido_jugador) {
        this.apellido_jugador = apellido_jugador;
    }

    public int getTel_jugador() {
        return tel_jugador;
    }

    public void setTel_jugador(int tel_jugador) {
        this.tel_jugador = tel_jugador;
    }

    public int getDias_entrenados() {
        return dias_entrenados;
    }

    public void setDias_entrenados(int dias_entrenados) {
        this.dias_entrenados = dias_entrenados;
    }

}
