/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.ResultSet;

/**
 *
 * @author Pablo
 */
public interface JugadorDOD {

     public ResultSet selectAllJugador();
    public JugadorDOD insertJugador(int id_jugador, String nombre_jugador, String apellido_jugador, int tel_jugador, int dias_entrenados);
    public boolean deleteJugador(int id_jugador);
    public JugadorDOD updateJugador(String nombre_jugador);
    // extra
    public ResultSet selectJugador(String nombre_jugador);
}
