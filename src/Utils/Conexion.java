/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Pablo
 */
public class Conexion {
    private String cadena_conexion = "jdbc:mysql://localhost:3306/equipofutbol";
    private String usuario = "root";
    private String password = "1234";

    public Conexion(String cadena_conexion, String usuario, String contrasena){
        this.cadena_conexion=cadena_conexion;
        this.usuario=usuario;
        this.password=contrasena;
        
    }
    
    public Connection getConnection() throws SQLException{
        try{
            Connection conn = DriverManager.getConnection(this.cadena_conexion, this.usuario, this.password);
            return conn;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public void closeConnection(Connection conn){
        try{
            conn.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public String getCadena_conexion() {
        return cadena_conexion;
    }

    public void setCadena_conexion(String cadena_conexion) {
        this.cadena_conexion = cadena_conexion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    

    

    
}
