package Utils;



import javax.swing.plaf.synth.Region;
import java.sql.*;

public class Pruebas {

    public static void main(String[] args) {
        // characterEncoding=UTF-8
        //String cadena_conexion = "jdbc:mysql://localhost:3306/hr?useUnicode=true&serverTimezone=UTC";

        String cadena_conexion = "jdbc:mysql://localhost:3306/equipofutbol";
        String usuario = "root";
        String contrasena = "1234";

        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from jugador");
            ResultSet rs = stm.executeQuery();

            System.out.println("Tabla jugador:");
            while (rs.next()) {
                int id = rs.getInt("id_jugador");
                String nombre = rs.getString("nombre_jugador");
                String apellido = rs.getString("apellido_jugador");
                int tel_jugador = rs.getInt("tel_jugador");
                int diasjugados = rs.getInt("dias_entrenados");

                System.out.println(id + " " + nombre + " " + apellido + " " + tel_jugador + " " + diasjugados);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("--------------------------------------");

        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from entrenador");
            ResultSet rs = stm.executeQuery();

            System.out.println("Tabla entrenador:");

            while (rs.next()) {
                int id = rs.getInt("id_entrenador");
                String nombre = rs.getString("nombre_entrenador");
                String campo = rs.getString("campo_entrenador");
                int num = rs.getInt("num_jugadores");
                int balones = rs.getInt("num_balones");

                System.out.println(id + " " + nombre + " " + campo + " " + num + " " + balones);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        System.out.println("--------------------------------------");
        
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from entrenar");
            ResultSet rs = stm.executeQuery();

            System.out.println("Tabla entrenar:");
            while (rs.next()) {
                int id = rs.getInt("id_jugador");
                int id1 = rs.getInt("id_entrenador");
                

                System.out.println(id + " " + id1);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
