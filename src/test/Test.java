package test;

import Utils.Conexion;
import com.sun.jndi.ldap.Connection;
import controler.EntrenadorControler;
import controler.EntrenarControler;
import controler.JugadorControler;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Entrenador;
import models.Entrenar;
import models.Jugador;

/**
 *
 * @author Pablo
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Conexion conexion = new Conexion();
        try {
            java.sql.Connection conn = conexion.getConnection();

            JugadorControler jg = new JugadorControler(conn);
            List<Jugador> lista_Jugador = jg.selectAllJugador();
            for(Jugador j: lista_Jugador){
                System.out.println(j);
            }
            jg.insertJugador(13, "Pablo","Gil",123,1);
            jg.updateJugador("Jose", 13);
            jg.deleteJugador(13);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            java.sql.Connection conn = conexion.getConnection();

            EntrenadorControler ec = new EntrenadorControler(conn);
            List<Entrenador> lista_Entrenador = ec.selectAllEntrenador();
            for(Entrenador e: lista_Entrenador){
                System.out.println(e);
            }
            ec.insertEntrenador(20, "Manuel","Vidal",123,1);
            ec.updateEntrenador("Jose", 20);
            ec.deleteEntrenador(20);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            java.sql.Connection conn = conexion.getConnection();

            EntrenarControler eec = new EntrenarControler(conn);
            List<Entrenar> lista_Entrenar = eec.selectAllEntrenar();
            for(Entrenar ee: lista_Entrenar){
                System.out.println(ee);
            }
            eec.insertEntrenar(49100357,49123456);
            eec.updateEntrenar(49100357, 1234);
            eec.deleteEntrenar(49123456);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

}
