/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Entrenador;
import models.EntrenadorDOD;
import models.Entrenar;

/**
 *
 * @author Pablo
 */
public class EntrenadorControler implements EntrenadorDOD {

    private final String SELECT_ALL = "select * from entrenador";
    private Connection con;

    public EntrenadorControler(Connection conn) {
        super();
        this.con=conn;
                
    }

    @Override
    public ArrayList<Entrenador> selectAllEntrenador() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Entrenador> lista = new ArrayList<Entrenador>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            while (rs.next()) {
                Entrenador e = new Entrenador(rs.getInt("id_entrenador"),rs.getString("nombre_entrenador"),rs.getString("campo_entrenador"),rs.getInt("num_jugadores"),rs.getInt("num_balones"));
                lista.add(e);
            }
            //rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return (ArrayList<Entrenador>) lista;
    }

    @Override
    public EntrenadorDOD insertEntrenador(int id_entrenador, String nombre_entrenador, String campo_entrenador, int num_jugadores, int num_balones) {
        String insert = "insert into entrenador (id_entrenador, nombre_entrenador, campo_entrenador, num_jugadores, num_balones) values(?,?,?,?,?)";

        try {
            PreparedStatement stm = con.prepareStatement(insert);
            stm.setInt(1, id_entrenador);
            stm.setString(2, nombre_entrenador);
            stm.setString(3, campo_entrenador);
            stm.setInt(4, num_jugadores);
            stm.setInt(5, num_balones);
            int rowsInserted = stm.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Un nuevo entrenador ha sido introducido");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteEntrenador(int id_entrenador) {
        String sql = "DELETE FROM entrenador WHERE id_entrenador=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id_entrenador);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El entrenador " + id_entrenador + " ha sido eliminado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public EntrenadorDOD updateEntrenador(String nombre_entrenador, int id_entrenador) {
        String sql = "UPDATE entrenador SET nombre_entrenador=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre_entrenador);
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se a actualizado el nombre del entrenador a " + nombre_entrenador);
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectEntrenador(String nombre_entrenador) {
        return null;
    }

}
