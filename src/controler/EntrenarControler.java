/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Entrenar;
import models.EntrenarDOD;
import models.Jugador;


/**
 *
 * @author Pablo
 */
public class EntrenarControler implements EntrenarDOD{

    private final String SELECT_ALL = "select * from entrenar";
    private Connection con;

    public EntrenarControler(Connection con) {
        super();        
        this.con = con;
    }    
    
    @Override
    public ArrayList<Entrenar> selectAllEntrenar() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Entrenar> lista = new ArrayList<Entrenar>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            while (rs.next()) {
                Entrenar ee = new Entrenar(rs.getInt("id_jugador"),rs.getInt("id_entrenador"));
                lista.add(ee);
            }
            //rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return (ArrayList<Entrenar>) lista;
    }

    @Override
    public EntrenarDOD insertEntrenar(int id_jugador, int id_entrenador) {
        String insert = "insert into entrenar (id_jugador,id_entrenador) values(?,?)";

        try {
            PreparedStatement stm = con.prepareStatement(insert);
            stm.setInt(1, id_jugador);
            stm.setInt(2, id_entrenador);
            int rowsInserted = stm.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Un nuevo campo de entrenar ha sido introducido");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteEntrenar(int id_jugador) {
        String sql = "DELETE FROM entrenar WHERE id_jugador=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1,id_jugador);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El campo entrenar con el id del jugador "+id_jugador+" ha sido eliminado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public EntrenarDOD updateEntrenar(int id_jugador, int id_entrenador) {
        String sql = "UPDATE entrenar SET id_jugador=? where id_entrenador=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id_jugador);
            statement.setInt(2, id_entrenador);
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se a actualizado el id del jugador a "+ id_jugador);
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectRegion(int id_jugador, int id_entrenador) {
        return null;
    }
    
}
