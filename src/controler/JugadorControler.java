/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Jugador;
import models.JugadorDOD;

/**
 *
 * @author Pablo
 */
public class JugadorControler implements JugadorDOD {

    private final String SELECT_ALL = "select * from jugador";
    private Connection con;

    public JugadorControler(Connection conn) {
        super();
        this.con=conn;
                
    }

    @Override
    public ArrayList<Jugador> selectAllJugador() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Jugador> lista = new ArrayList<Jugador>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            while (rs.next()) {
                Jugador j = new Jugador(rs.getInt("id_jugador"), rs.getString("nombre_jugador"), rs.getString("apellido_jugador"), rs.getInt("tel_jugador"), rs.getInt("dias_entrenados"));
                lista.add(j);
            }
            //rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return (ArrayList<Jugador>) lista;
    }

    @Override
    public JugadorDOD insertJugador(int id_jugador, String nombre_jugador, String apellido_jugador, int tel_jugador, int dias_entrenados) {
        String insert = "insert into jugador (id_jugador,nombre_jugador,apellido_jugador,tel_jugador,dias_entrenados) values(?,?,?,?,?)";

        try {
            PreparedStatement stm = con.prepareStatement(insert);
            stm.setInt(1, id_jugador);
            stm.setString(2, nombre_jugador);
            stm.setString(3, apellido_jugador);
            stm.setInt(4, tel_jugador);
            stm.setInt(5, dias_entrenados);
            int rowsInserted = stm.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Un nuevo jugador ha sido introducido");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }

    @Override
    public boolean deleteJugador(int id_jugador) {
        String sql = "DELETE FROM jugador WHERE id_jugador=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1,id_jugador );
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El jugador "+id_jugador+" ha sido eliminado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public JugadorDOD updateJugador(String nombre_jugador, int id_jugador) {
        String sql = "UPDATE jugador SET nombre_jugador=? WHERE id_jugador=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre_jugador);
            statement.setInt(2, id_jugador);
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se a actualizado el nombre del jugador a "+ nombre_jugador);
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectJugador(String nombre_jugador) {
        
        return null;
    }

}
